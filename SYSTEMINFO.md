# Centreon

Vendor: Centreon
Homepage: https://www.centreon.com/

Product: Centreon
Product Page: https://www.centreon.com/platform-overview/

## Introduction
We classify Centreon into the Service Assurance domain as Centreon provides a IT performance monitoring solution that helps to ensure availability and performance of IT infrastructure.

## Why Integrate
The Centreon adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Centreon. With this adapter you have the ability to perform operations such as:

- Get Hosts
- Get Host Categories
- Get Host Groups
- Get Host Severities
- Get Services
- Get Service
- Get Service Groups
- Get Time Periods
- Create a Service Severity
- Get a Service Severity

## Additional Product Documentation
[API Documents for Centreon](https://docs-api.centreon.com/api/centreon-web/23.04/)