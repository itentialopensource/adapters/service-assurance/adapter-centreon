
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_19:29PM

See merge request itentialopensource/adapters/adapter-centreon!11

---

## 0.3.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-centreon!9

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:29PM

See merge request itentialopensource/adapters/adapter-centreon!8

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:27PM

See merge request itentialopensource/adapters/adapter-centreon!7

---

## 0.3.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/service-assurance/adapter-centreon!6

---

## 0.2.3 [03-25-2024]

* Tab Documents

See merge request itentialopensource/adapters/service-assurance/adapter-centreon!5

---

## 0.2.2 [03-06-2024]

* Update Metadata

See merge request itentialopensource/adapters/service-assurance/adapter-centreon!4

---

## 0.2.1 [02-26-2024]

* Changes made at 2024.02.26_11:05AM

See merge request itentialopensource/adapters/service-assurance/adapter-centreon!2

---

## 0.2.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/service-assurance/adapter-centreon!1

---
