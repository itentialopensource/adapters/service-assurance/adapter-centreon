## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Centreon. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Centreon.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Centreon. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listallacknowledgements(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all acknowledgements</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">massivelydisacknowledgeresources(callback)</td>
    <td style="padding:15px">Massively disacknowledge resources</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">displayoneacknowledgement(acknowledgementId, callback)</td>
    <td style="padding:15px">Display one acknowledgement</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/acknowledgements/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhostsacknowledgements(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all hosts acknowledgements</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanacknowledgementonmultiplehosts(body, callback)</td>
    <td style="padding:15px">Add an acknowledgement on multiple hosts</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallservicesacknowledgements(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all services acknowledgements</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanacknowledgementonmultipleservices(body, callback)</td>
    <td style="padding:15px">Add an acknowledgement on multiple services</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallacknowledgementsofahost(search, limit, page, sortBy, hostId, callback)</td>
    <td style="padding:15px">List all acknowledgements of a host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanacknowledgementonchosenhost(hostId, body, callback)</td>
    <td style="padding:15px">Add an acknowledgement on chosen host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelanacknowledgementonahost(hostId, callback)</td>
    <td style="padding:15px">Cancel an acknowledgement on a host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallacknowledgementsofaservice(search, limit, page, sortBy, hostId, serviceId, callback)</td>
    <td style="padding:15px">List all acknowledgements of a service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanacknowledgementonchosenservice(hostId, serviceId, body, callback)</td>
    <td style="padding:15px">Add an acknowledgement on chosen service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelanacknowledgementonaservice(hostId, serviceId, callback)</td>
    <td style="padding:15px">Cancel an acknowledgement on a service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallacknowledgementsofametaservice(search, limit, page, sortBy, metaId, callback)</td>
    <td style="padding:15px">List all acknowledgements of a metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addanacknowledgementonchosenmetaservice(metaId, body, callback)</td>
    <td style="padding:15px">Add an acknowledgement on chosen metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelanacknowledgementonametaservice(metaId, callback)</td>
    <td style="padding:15px">Cancel an acknowledgement on a metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/acknowledgements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acknowledgemultipleresources(body, callback)</td>
    <td style="padding:15px">Acknowledge multiple resources</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listalldowntimesofametaservice(search, limit, page, sortBy, metaId, callback)</td>
    <td style="padding:15px">List all downtimes of a metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addadowntimeonametaservice(metaId, body, callback)</td>
    <td style="padding:15px">Add a downtime on a metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listalldowntimes(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all downtimes</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">displayonedowntime(downtimeId, callback)</td>
    <td style="padding:15px">Display one downtime</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/downtimes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">canceladowntime(downtimeId, callback)</td>
    <td style="padding:15px">Cancel a downtime</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/downtimes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhostsdowntimes(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all hosts downtimes</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addandowntimeonmultiplehosts(body, callback)</td>
    <td style="padding:15px">Add an downtime on multiple hosts</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallservicesdowntimes(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all services downtimes</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addandowntimeonmultipleservices(body, callback)</td>
    <td style="padding:15px">Add an downtime on multiple services</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listalldowntimesofahost(hostId, showService, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all downtimes of a host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addadowntimeonahost(hostId, body, callback)</td>
    <td style="padding:15px">Add a downtime on a host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listalldowntimesofaservice(search, limit, page, sortBy, hostId, serviceId, callback)</td>
    <td style="padding:15px">List all downtimes of a service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addadowntimeonaservice(hostId, serviceId, body, callback)</td>
    <td style="padding:15px">Add a downtime on a service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setdowntimeformultipleresources(body, callback)</td>
    <td style="padding:15px">Set downtime for multiple resources</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/downtime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallmetaservicesconfigurations(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all meta services configurations</td>
    <td style="padding:15px">{base_path}/{version}/configuration/metaservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getametaservice(metaId, callback)</td>
    <td style="padding:15px">Get a meta service</td>
    <td style="padding:15px">{base_path}/{version}/configuration/metaservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallmetaservicemetrics(search, limit, page, sortBy, metaId, callback)</td>
    <td style="padding:15px">List all meta service metrics</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkmetaservice(metaId, body, callback)</td>
    <td style="padding:15px">Check metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkmultiplehosts(body, callback)</td>
    <td style="padding:15px">Check multiple hosts</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkmultipleservices(body, callback)</td>
    <td style="padding:15px">Check multiple services</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkhost(hostId, body, callback)</td>
    <td style="padding:15px">Check host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkservice(hostId, serviceId, body, callback)</td>
    <td style="padding:15px">Check service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkresources(body, callback)</td>
    <td style="padding:15px">Check resources</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitaresulttoasinglemetaservice(metaId, body, callback)</td>
    <td style="padding:15px">Submit a result to a single metaservice</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/metaservices/{pathv1}/submit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitaresulttoasinglehost(hostId, body, callback)</td>
    <td style="padding:15px">Submit a result to a single host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/submit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitaresulttoasingleservice(hostId, serviceId, body, callback)</td>
    <td style="padding:15px">Submit a result to a single service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/submit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitresulttoresources(body, callback)</td>
    <td style="padding:15px">Submit result to resources</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/submit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getlocalproviderconfigurationwithpasswordsecuritypolicy(callback)</td>
    <td style="padding:15px">get local provider configuration with password security policy</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/local?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatelocalproviderconfiguration(body, callback)</td>
    <td style="padding:15px">update local provider configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/local?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getopenidproviderconfiguration(callback)</td>
    <td style="padding:15px">get openid provider configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/openid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateopenidproviderconfiguration(body, callback)</td>
    <td style="padding:15px">update openid provider configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/openid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSAMLproviderconfiguration(callback)</td>
    <td style="padding:15px">Get SAML provider configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/saml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSAMLproviderconfiguration(body, callback)</td>
    <td style="padding:15px">Update SAML provider configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/saml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getwebSsoproviderconfigurationinformation(callback)</td>
    <td style="padding:15px">get web-sso provider configuration information</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/web-sso?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatewebSsoproviderconfiguration(body, callback)</td>
    <td style="padding:15px">update web-sso provider configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/authentication/providers/web-sso?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">administrationparameters(callback)</td>
    <td style="padding:15px">Administration parameters</td>
    <td style="padding:15px">{base_path}/{version}/administration/parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createvaultconfiguration(vaultId, body, callback)</td>
    <td style="padding:15px">Create vault configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/vaults/{pathv1}/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listvaultconfigurationsbyprovider(vaultId, callback)</td>
    <td style="padding:15px">List vault configurations by provider</td>
    <td style="padding:15px">{base_path}/{version}/administration/vaults/{pathv1}/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatevaultconfiguration(vaultId, vaultConfigurationId, body, callback)</td>
    <td style="padding:15px">Update vault configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/vaults/{pathv1}/configurations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletevaultconfiguration(vaultId, vaultConfigurationId, callback)</td>
    <td style="padding:15px">Delete vault configuration</td>
    <td style="padding:15px">{base_path}/{version}/administration/vaults/{pathv1}/configurations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listonevaultconfigurationbyitsid(vaultId, vaultConfigurationId, callback)</td>
    <td style="padding:15px">List one vault configuration by its id</td>
    <td style="padding:15px">{base_path}/{version}/administration/vaults/{pathv1}/configurations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(body, callback)</td>
    <td style="padding:15px">Login</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">Logout</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateuserpassword(alias, body, callback)</td>
    <td style="padding:15px">Update user password</td>
    <td style="padding:15px">{base_path}/{version}/authentication/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">providersConfigurations(callback)</td>
    <td style="padding:15px">Providers Configurations</td>
    <td style="padding:15px">{base_path}/{version}/authentication/providers/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationtoprovider(providerConfigurationName, body, callback)</td>
    <td style="padding:15px">Authentication to provider</td>
    <td style="padding:15px">{base_path}/{version}/authentication/providers/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutSession(callback)</td>
    <td style="padding:15px">Logout Session</td>
    <td style="padding:15px">{base_path}/{version}/authentication/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gorgonepollerresponsestoken(pollerId, token, callback)</td>
    <td style="padding:15px">Gorgone poller responses token</td>
    <td style="padding:15px">{base_path}/{version}/gorgone/pollers/{pathv1}/responses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gorgonecommandonspecificpoller(pollerId, commandName, callback)</td>
    <td style="padding:15px">Gorgone command on specific poller</td>
    <td style="padding:15px">{base_path}/{version}/gorgone/pollers/{pathv1}/commands/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhosts(showService, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all hosts</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getahost(hostId, callback)</td>
    <td style="padding:15px">Get a host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listhostcategories(callback)</td>
    <td style="padding:15px">List host categories</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createahostcategory(body, callback)</td>
    <td style="padding:15px">Create a host category</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteahostcategory(hostCategoryId, callback)</td>
    <td style="padding:15px">Delete a host category</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteaservicecategory(serviceCategoryId, callback)</td>
    <td style="padding:15px">Delete a service category</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallrealTimehostcategories(callback)</td>
    <td style="padding:15px">List all real-time host categories</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhostgroups(callback)</td>
    <td style="padding:15px">List all host groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addahostgroup(body, callback)</td>
    <td style="padding:15px">Add a host group</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteahostgroup(hostgroupId, callback)</td>
    <td style="padding:15px">Delete a host group</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhostgroupsbyhostid(hostId, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all host groups by host id</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/hostgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListallhostgroups(showHost, showService, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all host groups</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hostgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhostseverities(callback)</td>
    <td style="padding:15px">List all host severities</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/severities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createahostseverity(body, callback)</td>
    <td style="padding:15px">Create a host severity</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/severities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteahostseverity(hostSeverityId, callback)</td>
    <td style="padding:15px">Delete a host severity</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/severities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhosttemplates(callback)</td>
    <td style="padding:15px">List all host templates</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallmonitoringserversconfigurations(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all monitoring servers configurations</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generatetheconfigurationofthemonitoringserver(monitoringServerId, callback)</td>
    <td style="padding:15px">Generate the configuration of the monitoring server</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers/{pathv1}/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reloadtheconfigurationofthemonitoringserver(monitoringServerId, callback)</td>
    <td style="padding:15px">Reload the configuration of the monitoring server</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers/{pathv1}/reload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateandreloadtheconfigurationofthemonitoringserver(monitoringServerId, callback)</td>
    <td style="padding:15px">Generate and reload the configuration of the monitoring server</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers/{pathv1}/generate-and-reload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generatetheconfigurationforallmonitoringservers(callback)</td>
    <td style="padding:15px">Generate the configuration for all monitoring servers</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reloadtheconfigurationforallmonitoringservers(callback)</td>
    <td style="padding:15px">Reload the configuration for all monitoring servers</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers/reload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateandreloadtheconfigurationforallmonitoringservers(callback)</td>
    <td style="padding:15px">Generate and reload the configuration for all monitoring servers</td>
    <td style="padding:15px">{base_path}/{version}/configuration/monitoring-servers/generate-and-reload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallrealtimemonitoringservers(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all real time monitoring servers</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">displaythedefaultconfigurationoftheCentreonproxy(callback)</td>
    <td style="padding:15px">Display the default configuration of the Centreon proxy</td>
    <td style="padding:15px">{base_path}/{version}/configuration/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatethedefaultconfigurationoftheCentreonproxy(body, callback)</td>
    <td style="padding:15px">Update the default configuration of the Centreon proxy</td>
    <td style="padding:15px">{base_path}/{version}/configuration/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallservices(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all services</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listservicesrelatedtoahost(hostId, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List services related to a host</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaservice(hostId, serviceId, callback)</td>
    <td style="padding:15px">Get a service</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallservicegroups(callback)</td>
    <td style="padding:15px">List all service groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addaservicegroup(body, callback)</td>
    <td style="padding:15px">Add a service group</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteahostgroup1(servicegroupId, callback)</td>
    <td style="padding:15px">Delete a host group</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getservicegroupsbyhostidandserviceid(hostId, serviceId, callback)</td>
    <td style="padding:15px">Get service groups by host id and service id</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/servicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListallservicegroups(showService, showHost, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all service groups</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/servicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listservicecategories(callback)</td>
    <td style="padding:15px">List service categories</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createaservicecategory(body, callback)</td>
    <td style="padding:15px">Create a service category</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallrealTimeservicecategories(callback)</td>
    <td style="padding:15px">List all real-time service categories</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/services/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallserviceseveritiesfromtherealtime(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all service severities from the realtime</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/severities/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallhostseveritiesfromrealTimedata(search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">List all host severities from real-time data</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/severities/host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getalltimeperiods(callback)</td>
    <td style="padding:15px">Get all time periods</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timeperiods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewtimeperiod(body, callback)</td>
    <td style="padding:15px">Create a new time period</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timeperiods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteatimeperiod(id, callback)</td>
    <td style="padding:15px">Delete a time period</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timeperiods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getatimeperiod(id, callback)</td>
    <td style="padding:15px">Get a time period</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timeperiods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateatimeperiod(id, body, callback)</td>
    <td style="padding:15px">Update a time period</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timeperiods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registertoplatformtopology(body, callback)</td>
    <td style="padding:15px">Register to platform topology</td>
    <td style="padding:15px">{base_path}/{version}/platform/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaplatformtopology(callback)</td>
    <td style="padding:15px">Get a platform topology</td>
    <td style="padding:15px">{base_path}/{version}/platform/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteaPlatform(pollerId, callback)</td>
    <td style="padding:15px">Delete a Platform</td>
    <td style="padding:15px">{base_path}/{version}/platform/topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallserviceseverities(callback)</td>
    <td style="padding:15px">List all service severities</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/severities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createaserviceseverity(body, callback)</td>
    <td style="padding:15px">Create a service severity</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/severities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteaserviceseverity(serviceSeverityId, callback)</td>
    <td style="padding:15px">Delete a service severity</td>
    <td style="padding:15px">{base_path}/{version}/configuration/services/severities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallicons(callback)</td>
    <td style="padding:15px">List all icons</td>
    <td style="padding:15px">{base_path}/{version}/configuration/icons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnotificationpolicyofahost(hostId, callback)</td>
    <td style="padding:15px">Get notification policy of a host</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/{pathv1}/notification-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnotificationpolicyofaservice(hostId, serviceId, callback)</td>
    <td style="padding:15px">Get notification policy of a service</td>
    <td style="padding:15px">{base_path}/{version}/configuration/hosts/{pathv1}/services/{pathv2}/notification-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnotificationpolicyofametaservice(metaId, callback)</td>
    <td style="padding:15px">Get notification policy of a meta service</td>
    <td style="padding:15px">{base_path}/{version}/configuration/metaservices/{pathv1}/notification-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallowedactions(callback)</td>
    <td style="padding:15px">List allowed actions</td>
    <td style="padding:15px">{base_path}/{version}/users/acl/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getconfiguredusers(callback)</td>
    <td style="padding:15px">Get configured users</td>
    <td style="padding:15px">{base_path}/{version}/configuration/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listuserparameters(callback)</td>
    <td style="padding:15px">List user parameters</td>
    <td style="padding:15px">{base_path}/{version}/configuration/users/current/parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateauser(body, callback)</td>
    <td style="padding:15px">Update a user</td>
    <td style="padding:15px">{base_path}/{version}/configuration/users/current/parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listcontacttemplates(callback)</td>
    <td style="padding:15px">List contact templates</td>
    <td style="padding:15px">{base_path}/{version}/configuration/contacts/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listcontactgroups(callback)</td>
    <td style="padding:15px">List contact groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration/contacts/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listuserfiltersbypage(pageName, callback)</td>
    <td style="padding:15px">List user filters by page</td>
    <td style="padding:15px">{base_path}/{version}/users/filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detaileduserfilter(pageName, filterId, callback)</td>
    <td style="padding:15px">Detailed user filter</td>
    <td style="padding:15px">{base_path}/{version}/users/filters/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adduserfilter(pageName, body, callback)</td>
    <td style="padding:15px">Add user filter</td>
    <td style="padding:15px">{base_path}/{version}/users/filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateuserfilter(pageName, filterId, body, callback)</td>
    <td style="padding:15px">Update user filter</td>
    <td style="padding:15px">{base_path}/{version}/users/filters/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchuserfilter(pageName, filterId, body, callback)</td>
    <td style="padding:15px">Patch user filter</td>
    <td style="padding:15px">{base_path}/{version}/users/filters/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteuserfilter(pageName, filterId, callback)</td>
    <td style="padding:15px">Delete user filter</td>
    <td style="padding:15px">{base_path}/{version}/users/filters/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listaccessgroups(callback)</td>
    <td style="padding:15px">List access groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration/access-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallresourcesincludinghostsandservices(search, limit, page, sortBy, types, states, statuses, hostgroupNames, servicegroupNames, serviceCategoryNames, hostCategoryNames, hostSeverityNames, serviceSeverityNames, hostSeverityLevels, serviceSeverityLevels, monitoringServerNames, statusTypes, callback)</td>
    <td style="padding:15px">List all resources including hosts and services</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaHostresourcetypedetail(hostId, callback)</td>
    <td style="padding:15px">Get a Host resource type detail</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaServiceresourcetypedetail(hostId, serviceId, callback)</td>
    <td style="padding:15px">Get a Service resource type detail</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/hosts/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addcommenttoresources(body, callback)</td>
    <td style="padding:15px">Add comment to resources</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/resources/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getservicemetrics(hostId, serviceId, start, end, callback)</td>
    <td style="padding:15px">Get service metrics</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/metrics/start/{pathv3}/end/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getservicestatusdata(hostId, serviceId, start, end, callback)</td>
    <td style="padding:15px">Get service status data</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/status/start/{pathv3}/end/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getserviceperformancedata(hostId, serviceId, start, end, callback)</td>
    <td style="padding:15px">Get service performance data</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/metrics/performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadperformancedataascsvfile(hostId, serviceId, startDate, end, callback)</td>
    <td style="padding:15px">Download performance data as csv file</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/metrics/performance/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getservicestatusdata1(hostId, serviceId, start, end, callback)</td>
    <td style="padding:15px">Get service status data1</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/metrics/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gethosttimeline(hostId, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">Get host timeline</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/timeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getservicetimeline(hostId, serviceId, search, limit, page, sortBy, callback)</td>
    <td style="padding:15px">Get service timeline</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/hosts/{pathv1}/services/{pathv2}/timeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateaplatforminformation(body, callback)</td>
    <td style="padding:15px">Update a platform information</td>
    <td style="padding:15px">{base_path}/{version}/platform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listversionsofwebWidgetsandmodules(callback)</td>
    <td style="padding:15px">List versions of web, widgets and modules</td>
    <td style="padding:15px">{base_path}/{version}/platform/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCentreonweb(body, callback)</td>
    <td style="padding:15px">Update Centreon web</td>
    <td style="padding:15px">{base_path}/{version}/platform/updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcentreonwebversions(callback)</td>
    <td style="padding:15px">Get centreon web versions</td>
    <td style="padding:15px">{base_path}/{version}/platform/installation/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
