# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Centreon System. The API that was used to build the adapter for Centreon is usually available in the report directory of this adapter. The adapter utilizes the Centreon API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Centreon adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Centreon. With this adapter you have the ability to perform operations such as:

- Get Hosts
- Get Host Categories
- Get Host Groups
- Get Host Severities
- Get Services
- Get Service
- Get Service Groups
- Get Time Periods
- Create a Service Severity
- Get a Service Severity

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
